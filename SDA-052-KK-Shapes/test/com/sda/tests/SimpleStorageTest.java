package com.sda.tests;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.sda.data.structures.Circle;
import com.sda.data.structures.SimpleStorage;
import com.sda.data.structures.Square;
import com.sda.shapes.Shape;

public class SimpleStorageTest {

	@Test
	public void testAddingToStorage() {

	}

	@Test
	public void testPrintEntries() {

	}

	@Test
	public void testDeleteShapeFromList() {
		SimpleStorage simpleStorage = new SimpleStorage();

		Square square2 = new Square(2);
		Square square4 = new Square(4);
		Square square4_2 = new Square(4);
		Circle circle1 = new Circle(5);
		

		simpleStorage.addShapeToList(square2);
		simpleStorage.addShapeToList(square4);
		simpleStorage.addShapeToList(square4_2);
		simpleStorage.addShapeToList(circle1);

		simpleStorage.printList();
		System.out.println();

		simpleStorage.deleteShapeFromList(16);

		simpleStorage.printList();
		System.out.println();

		simpleStorage.countShapeTypesOccurrences();
	}

}
