package com.sda.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sda.data.structures.Circle;

public class AreaCalculatorTest {

	
	@Test
	public void testCircleArea(){
		
		Circle circle = new Circle(2);
		
		assertTrue(circle.area() > 12 && circle.area() < 13);
		
	}
	
	@Test
	public void testRectangleArea(){
		
	}
	
	@Test
	public void testSquareArea(){

	}

}
