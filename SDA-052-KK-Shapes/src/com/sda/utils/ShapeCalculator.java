package com.sda.utils;

import com.sda.shapes.Shape;

public class ShapeCalculator {

	public static double calculateArea(Shape shape) {

		return shape.area();

	}

}
