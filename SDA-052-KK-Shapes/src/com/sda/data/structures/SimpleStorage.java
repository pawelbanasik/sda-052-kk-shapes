package com.sda.data.structures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sda.shapes.Shape;

public class SimpleStorage implements AbstractStorage {

	private List<Shape> list = new ArrayList<>();

	public SimpleStorage() {

	}

	@Override
	public void addShapeToList(Shape shape) {
		list.add(shape);

	}

	@Override
	public void deleteShapeFromList(double area) {
		//
		// for (Shape shape : list) {
		// if (shape.area() == area) {
		// list.remove(shape);
		// }
		// }
		Iterator<Shape> iter = list.iterator();

		while (iter.hasNext()) {
			Shape s = iter.next();

			if (s.area() == area) {
				iter.remove();
			}
		}

	}

	public void printList() {
		for (Shape element : list) {
			System.out.println("Typ: " + element.getClass().getSimpleName() + " Pole " + element.area());
		}

	}

	public void countShapeTypesOccurrences() {

		int squareCount = 0;
		int circleCount = 0;
		int rectangleCount = 0;

		for (Shape shape : list) {
			if (shape.getClass().equals(Square.class)) {
				squareCount++;
			}

			else if (shape.getClass().equals(Rectangle.class)) {
				rectangleCount++;
			}

			else if (shape.getClass().equals(Circle.class)) {
				circleCount++;
			}

		}
		System.out.println("Square number: " + squareCount);
		System.out.println("Rectangle number: " + rectangleCount);
		System.out.println("Circle number: " + circleCount);
	}

}
