package com.sda.data.structures;

import com.sda.shapes.Shape;

public class Circle implements Shape {

	private double radius;
	
	
	
	public Circle(double radius) {
		super();
		this.radius = radius;
	}

	@Override
	public double area() {
		
		return Math.PI*radius*radius;
	}

	@Override
	public double perimeter() {
		
		return 2*Math.PI*radius;
	}

	
	
}
