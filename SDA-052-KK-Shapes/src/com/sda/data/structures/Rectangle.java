package com.sda.data.structures;

import com.sda.shapes.Shape;

public class Rectangle implements Shape{

	private double sideA;
	private double sideB;
	
	public Rectangle(double sideA, double sideB) {
		super();
		this.sideA = sideA;
		this.sideB = sideB;
	}

	@Override
	public double area() {
		
		return sideA*sideB;
	}

	@Override
	public double perimeter() {
	
		return 2*sideA+2*sideB;
	}
	
	
	
	
	
	
	
}
