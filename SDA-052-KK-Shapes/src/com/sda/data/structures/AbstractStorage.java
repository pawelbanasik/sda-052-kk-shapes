package com.sda.data.structures;

import com.sda.shapes.Shape;

public interface AbstractStorage {
	
	void addShapeToList(Shape shape);
	
	void deleteShapeFromList(double area);
	
}
